//IMPORTANT: LANGUAGES, GT_CODE and LOCAL_CODE
//needs to be perfectly sinchronized.
//Also , LANGUAGES first column is identical
//to LOCAL_CODE.
//Whoever did the LANGUAGES array
//didn't think too well, over working and making unnecesary calculation
//with a double value matrix.
//This is make changing array utterly complex

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.File;
import java.util.Vector;

public class DataPack
{
    public static final int MAX_TITLE_LENGTH = 50;
    public static final int MAX_SHORT_DESCRIPTION = 80;
    public static final int MAX_LONG_DESCRIPTION = 4000;
    public String sPackageName;
    public String sGoogleServiceAccountEmail;
    public String sGoogleServiceProjectName;
    public String[] sLanguagesWanted;
    public Vector<TransData> arrTrans;
    private String[] GT_CODE;
    private String[] LOCALE_CODE;
    private static String[] LANGUAGES;
    
    static {
        DataPack.LANGUAGES = new String[] {
                "en-US", "English (USA / DEFAULT)",
                "af", "Afrikaans",
                "am", "Amharic",
                "ar", "Arabic",
                "hy-AM", "Armenian",
                "az", "Azerbaijani",
                "eu-ES", "Basque",
                "be", "Belarusian",
                "bn-BD", "Bengali",
                "my-MM", "Burmese (Myanmar)",
                "bg", "Bulgarian",
                "ca", "Catalan",
                "zh-CN", "Chinese (Simplified)",
                "zh-TW", "Chinese (Traditional)",
                "hr", "Croatian",
                "cs", "Czech",
                "da", "Danish",
                "nl", "Dutch",
                "en-GB", "English (UK)",
                "en-AU", "English (Australia)",
                "en-IN", "English (India)",
                "et", "Estonian",
                "tl", "Filipino",
                "fi", "Finnish",
                "fr", "French",
                "fr-CA", "French (Canada)",
                "gl-ES", "Galician",
                "de", "German",
                "ka-GE", "Georgian",
                "el", "Greek",
                "iw", "Hebrew",
                "hi", "Hindi",
                "hu", "Hungarian",
                "is-IS", "Icelandic",
                "id", "Indonesian",
                "it", "Italian",
                "ja", "Japanese",
                "kn-IN", "Kannada",
                "km-KH", "Khmer",
                "ko", "Korean",
                "ky-KG", "Kyrgiz",
                "lo-LA", "Lao",
                "lv", "Latvian",
                "lt", "Lithuanian",
                "mk-MK", "Macedonian",
                "ml-IN", "Malayalam",
                "mr-IN", "Marathi",
                "ms", "Malay",
                "mn-MN", "Mongolian",
                "ne-NP", "Nepali",
                "no", "Norwegian",
                "fa", "Persian",
                "pl", "Polish",
                "pt", "Portuguese",
                "pt-BR", "Portuguese (Brazil)",
                "ro", "Romanian",
                "ru", "Russian",
                "sr", "Serbian",
                "sk", "Slovak",
                "sl", "Slovenian",
                "es", "Spanish",
                "es-419", "Spanish (Latin)",
                "es-US", "Spanish (USA)",
                "sw", "Swahili",
                "sv", "Swedish",
                "ta-IN", "Tamil",
                "te-IN", "Telugu",
                "th", "Thai",
                "tr", "Turkish",
                "uk", "Ukrainian",
                "vi", "Vietnamese",
                "zu", "Zulu" };
    }
    
    public int getAllLanguagesCnt() {
        return DataPack.LANGUAGES.length / 2;
    }
    
    public String getLanguagePref(final int n) {
        return DataPack.LANGUAGES[n * 2];
    }
    
    public String getLanguageName(final int n) {
        return DataPack.LANGUAGES[n * 2 + 1];
    }
    
    public String[] getLangComboNames() {
        final String[] a = new String[this.arrTrans.size()];
        for (int x = 0; x < a.length; ++x) {
            final String sLangCode = this.arrTrans.get(x).sLanguageCode;
            a[x] = sLangCode;
            for (int y = 0; y < DataPack.LANGUAGES.length; y += 2) {
                if (DataPack.LANGUAGES[y].equals(sLangCode)) {
                    a[x] = DataPack.LANGUAGES[y + 1];
                }
            }
        }
        return a;
    }
    
    public String[] getNotPresentLangComboNames() {
        final String[] a = new String[DataPack.LANGUAGES.length / 2 - this.arrTrans.size()];
        int k = 0;
        for (int y = 2; y < DataPack.LANGUAGES.length; y += 2) {
            final String sLangCode = DataPack.LANGUAGES[y];
            int flag = 0;
            for (int x = 1; x < this.arrTrans.size(); ++x) {
                if (this.arrTrans.get(x).sLanguageCode.equals(sLangCode)) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0 && k < a.length) {
                a[k] = DataPack.LANGUAGES[y + 1];
                ++k;
            }
        }
        return a;
    }
    
    public void setLang_Title(final int n, final String v) {
        this.arrTrans.get(n).sTitle = v;
    }
    
    public void setLang_ShortDesc(final int n, final String v) {
        this.arrTrans.get(n).sShortDesc = v;
    }
    
    public void setLang_LongDesc(final int n, final String v) {
        this.arrTrans.get(n).sLongDesc = v;
    }
    
    public String getLang_Title(final int n) {
        return this.arrTrans.get(n).sTitle;
    }
    
    public String getLang_ShortDesc(final int n) {
        return this.arrTrans.get(n).sShortDesc;
    }
    
    public String getLang_LongDesc(final int n) {
        return this.arrTrans.get(n).sLongDesc;
    }
    
    public String getLang_Code(final int n) {
        return this.arrTrans.get(n).sLanguageCode;
    }
    
    public String getLang_Locale(final int n) {
        return this.arrTrans.get(n).sLocale;
    }
    
    public String getLang_GTCode(final int n) {
        return this.arrTrans.get(n).sLangGT;
    }
    
    public int getLang_ArrCount() {
        return this.arrTrans.size();
    }
    
    public String getLocaleLangCode(final String sLangCode) {
        for (int i = 0; i < DataPack.LANGUAGES.length / 2; ++i) {
            if (DataPack.LANGUAGES[i * 2].equals(sLangCode)) {
                return this.LOCALE_CODE[i];
            }
        }
        return "";
    }
    
    public String getLocaleLangName(final String sLangCode) {
        for (int i = 0; i < DataPack.LANGUAGES.length / 2; ++i) {
            if (DataPack.LANGUAGES[i * 2].equals(sLangCode)) {
                return DataPack.LANGUAGES[i * 2 + 1];
            }
        }
        return "";
    }
    
    public String getGTLangCode(final String sLangCode) {
        for (int i = 0; i < DataPack.LANGUAGES.length / 2; ++i) {
            if (DataPack.LANGUAGES[i * 2].equals(sLangCode)) {
                return this.GT_CODE[i];
            }
        }
        return "en";
    }
    
    public DataPack() {
        this.arrTrans = new Vector<TransData>();
        this.GT_CODE = new String[] {
                "en", "af", "am", "ar", "hy", "az", "eu", "be", "bn", "my",
                "bg", "ca", "zh-CN", "zh-TW", "hr", "cs", "da", "nl", "en",
                "en", "en", "et", "tl", "fi", "fr", "fr", "gl", "de", "ka",
                "el", "iw", "hi", "hu", "is", "id", "it", "ja", "kn", "km",
                "ko", "ky", "lo", "lv", "lt", "mk", "ml", "mr", "ms", "mn",
                "ne", "no", "fa", "pl", "pt", "pt", "ro", "ru", "sr", "sk",
                "sl", "es", "es", "es", "sw", "sv", "ta", "te", "th", "tr",
                "uk", "vi", "zu" };
        this.LOCALE_CODE = new String[] {
                "en-US", "af", "am", "ar", "hy-AM", "az-AZ", "eu-ES", "be", "bn-BD", "my-MM",
                "bg", "ca", "zh-CN", "zh-TW", "hr", "cs-CZ", "da-DK", "nl-NL", "en-GB", "en-AU",
                "en-IN", "et", "tl", "fi-FI", "fr-FR", "fr-CA", "gl-ES", "de-DE", "ka-GE", "el-GR",
                "iw-IL", "hi-IN", "hu-HU", "is-IS", "id", "it-IT", "ja-JP", "kn-IN", "km-KH", "ko-KR",
                "ky-KG", "lo-LA", "lv", "lt", "mk-MK", "ml-IN", "mr-IN", "ms", "mn-MN", "ne-NP",
                "no-NO", "fa", "pl-PL", "pt-PT", "pt-BR", "ro", "ru-RU", "sr", "sk", "sl",
                "es-ES", "es-419", "es-US", "sw", "sv-SE", "ta-IN", "te-IN", "th", "tr-TR",
                "uk", "vi", "zu" };
    }
    
    public String getSourceProjectDirectoryName() {
        try {
            return new File(".").getCanonicalPath();
        }
        catch (IOException e) {
            return "";
        }
    }
    
    public void loadFromFile(final String fName) {
        final TransData baseTrans = new TransData("en-US", "en-US", "en");
        this.arrTrans.add(baseTrans);
        boolean bLongDesc = false;
        int langId = 0;
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        try {
            final File file = new File(fName);
            try {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
            } catch (   FileNotFoundException fne) {
                System.out.println("Ooooops file "  + fName + " missing!!!");
            }

            String line;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (line.startsWith("[LongDescription")) {
                    bLongDesc = true;
                    String rest = line.substring(16);
                    if (rest.equals("]")) {
                        langId = 0;
                    }
                    else {
                        rest = rest.substring(1, rest.length() - 1);
                        for (int ix = 0; ix < this.arrTrans.size(); ++ix) {
                            if (this.arrTrans.get(ix).sLanguageCode.equals(rest)) {
                                langId = ix;
                            }
                        }
                    }
                }
                else if (bLongDesc) {
                    if (line.startsWith("[/LongDescription")) {
                        bLongDesc = false;
                        this.setLang_LongDesc(langId, sb.toString());
                        sb = new StringBuilder();
                    }
                    else {
                        if (sb.length() > 0) {
                            sb.append("\n");
                        }
                        sb.append(line);
                    }
                }
                else {
                    final int ix2 = line.indexOf(61);
                    String sName = "";
                    String sVal = line;
                    if (ix2 != -1) {
                        sName = line.substring(0, ix2);
                        sVal = line.substring(ix2 + 1);
                        sName = sName.trim();
                        sVal = sVal.trim();
                    }
                    if (sName.equals("PackageName")) {
                        this.sPackageName = sVal;
                    }
                    if (sName.equals("GoogleServiceAccountEmail")) {
                        this.sGoogleServiceAccountEmail = sVal;
                    }
                    if (sName.equals("GoogleServiceProjectName")) {
                        this.sGoogleServiceProjectName = sVal;
                    }
                    if (sName.startsWith("Title")) {
                        String rest2 = sName.substring(5);
                        if (rest2.length() == 0) {
                            langId = 0;
                        }
                        else {
                            rest2 = sName.substring(6);
                            for (int ih = 0; ih < this.arrTrans.size(); ++ih) {
                                if (this.arrTrans.get(ih).sLanguageCode.equals(rest2)) {
                                    langId = ih;
                                }
                            }
                        }
                        this.setLang_Title(langId, sVal);
                    }
                    if (sName.startsWith("ShortDesc")) {
                        String rest2 = sName.substring(9);
                        if (rest2.length() == 0) {
                            langId = 0;
                        }
                        else {
                            rest2 = sName.substring(10);
                            for (int ih = 0; ih < this.arrTrans.size(); ++ih) {
                                if (this.arrTrans.get(ih).sLanguageCode.equals(rest2)) {
                                    langId = ih;
                                }
                            }
                        }
                        this.setLang_ShortDesc(langId, sVal);
                    }
                    if (!sName.equals("Lang")) {
                        continue;
                    }
                    this.sLanguagesWanted = sVal.split(",");
                    for (int i = 0; i < this.sLanguagesWanted.length; ++i) {
                        if (this.sLanguagesWanted[i].length() > 0) {
                            this.arrTrans.add(new TransData(this.sLanguagesWanted[i], this.getLocaleLangCode(this.sLanguagesWanted[i]), this.getGTLangCode(this.sLanguagesWanted[i])));
                        }
                    }
                }
            }
            if (bLongDesc && sb.length() > 0) {
                bLongDesc = false;
                this.setLang_LongDesc(langId, sb.toString());
                sb = new StringBuilder();
            }
        }
        catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
        catch (Exception e3) {
            e3.printStackTrace();
        }
        try {
            br.close();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
    }
    
    public String getPackageName() {
        return this.sPackageName;
    }
    
    public String getGoogleServiceProjectName() {
        return this.sGoogleServiceProjectName;
    }
    
    public String getGoogleServiceAccountEmail() {
        return this.sGoogleServiceAccountEmail;
    }
    
    public String validation() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.getLang_ArrCount(); ++i) {
            if (this.getLang_Locale(i).length() > 0) {
                int n = BasicUploadApk.tagsclean(this.getLang_Title(i)).length();
                if (n > MAX_TITLE_LENGTH) {
                    sb.append("Title for ");
                    sb.append(this.getLocaleLangName(this.getLang_Code(i)));
                    sb.append(" (");
                    sb.append(this.getLang_Locale(i));
                    sb.append(") exceeds " + MAX_TITLE_LENGTH + " characters by ");
                    sb.append(Integer.toString(n - MAX_TITLE_LENGTH));
                    sb.append(".\n");
                }
                n = BasicUploadApk.tagsclean(this.getLang_ShortDesc(i)).length();
                if (n > MAX_SHORT_DESCRIPTION) {
                    sb.append("ShortDesc for ");
                    sb.append(this.getLocaleLangName(this.getLang_Code(i)));
                    sb.append(" (");
                    sb.append(this.getLang_Locale(i));
                    sb.append(") exceeds " + MAX_SHORT_DESCRIPTION + " characters by ");
                    sb.append(Integer.toString(n - MAX_SHORT_DESCRIPTION));
                    sb.append(".\n");
                }
                n = BasicUploadApk.tagsclean(this.getLang_LongDesc(i)).length();
                if (n > MAX_LONG_DESCRIPTION) {
                    sb.append("LongDesc for ");
                    sb.append(this.getLocaleLangName(this.getLang_Code(i)));
                    sb.append(" (");
                    sb.append(this.getLang_Locale(i));
                    sb.append(") exceeds " + MAX_LONG_DESCRIPTION + " characters by ");
                    sb.append(Integer.toString(n - MAX_LONG_DESCRIPTION));
                    sb.append(".\n");
                }
            }
        }
        return sb.toString();
    }
    
    public void addLangByName(final String input) {
        for (int i = 0; i < DataPack.LANGUAGES.length; i += 2) {
            final String sLangName = DataPack.LANGUAGES[i + 1];
            if (sLangName.equals(input)) {
                int j;
                for (j = 1; j < this.arrTrans.size(); ++j) {
                    final String sLangName2 = this.arrTrans.get(j).getLangName();
                    if (sLangName2.compareTo(sLangName) >= 0) {
                        break;
                    }
                }
                this.arrTrans.insertElementAt(new TransData(DataPack.LANGUAGES[i], this.getLocaleLangCode(DataPack.LANGUAGES[i]), this.getGTLangCode(DataPack.LANGUAGES[i])), j);
                break;
            }
        }
    }
    
    public void delLang(final int n) {
        this.arrTrans.remove(n);
    }
    
    public int saveToFile(final String sfn) {
        final String suffix = ".tmp.2";
        try {
            final PrintWriter writer = new PrintWriter(String.valueOf(sfn) + suffix, "UTF-8");
            writer.println("# APK Translator & Uploader v3.0 by FZSM");
            writer.println("# lines starting with '#' are comment lines");
            writer.println("# Warning: this file will be overwritten if you chooose save .gpup");
            writer.println("");
            writer.println("PackageName=" + this.sPackageName);
            writer.println("");
            writer.println("GoogleServiceProjectName=" + this.sGoogleServiceProjectName);
            writer.println("GoogleServiceAccountEmail=" + this.sGoogleServiceAccountEmail);
            writer.print("Lang=");
            for (int x = 1; x < this.arrTrans.size(); ++x) {
                if (x > 1) {
                    writer.print(",");
                }
                writer.print(this.arrTrans.get(x).sLanguageCode);
            }
            writer.println("");
            writer.println("");
            for (int x = 0; x < this.arrTrans.size(); ++x) {
                final TransData td = this.arrTrans.get(x);
                if (x == 0 || !td.sTitle.isEmpty() || !td.sShortDesc.isEmpty() || !td.sLongDesc.isEmpty()) {
                    if (x == 0) {
                        writer.print("Title=");
                    }
                    else {
                        writer.print("Title_" + td.sLanguageCode + "=");
                    }
                    writer.println(td.sTitle);
                    if (x == 0) {
                        writer.print("ShortDesc=");
                    }
                    else {
                        writer.print("ShortDesc_" + td.sLanguageCode + "=");
                    }
                    writer.println(td.sShortDesc);
                    if (x == 0) {
                        writer.println("[LongDescription]");
                    }
                    else {
                        writer.println("[LongDescription_" + td.sLanguageCode + "]");
                    }
                    writer.println(td.sLongDesc);
                    if (x == 0) {
                        writer.println("[/LongDescription]");
                    }
                    else {
                        writer.println("[/LongDescription_" + td.sLanguageCode + "]");
                    }
                    writer.println("");
                }
            }
            writer.close();
            final File f1 = new File(String.valueOf(sfn) + suffix);
            final File f2 = new File(sfn);
            if (f2.delete()) {
                f1.renameTo(f2);
                return 0;
            }
            return -1;
        }
        catch (Exception e) {
            return -1;
        }
    }
    
    class TransData
    {
        public String sLanguageCode;
        public String sLocale;
        public String sTitle;
        public String sShortDesc;
        public String sLongDesc;
        public String sLangGT;
        
        TransData(final String sLanguageCode, final String sLocale, final String l) {
            this.sLanguageCode = sLanguageCode;
            this.sLocale = sLocale;
            this.sLangGT = l;
            this.sTitle = "";
            this.sShortDesc = "";
            this.sLongDesc = "";
        }
        
        TransData(final String sLanguageCode, final String sLocale, final String sTitle, final String sShortDesc, final String sLongDesc) {
            this.sLanguageCode = sLanguageCode;
            this.sLocale = sLocale;
            this.sTitle = sTitle;
            this.sShortDesc = sShortDesc;
            this.sLongDesc = sLongDesc;
        }
        
        public String getLangName() {
            for (int i = 0; i < DataPack.LANGUAGES.length; i += 2) {
                if (DataPack.LANGUAGES[i].equals(this.sLanguageCode)) {
                    return DataPack.LANGUAGES[i + 1];
                }
            }
            return "??";
        }
    }
}
