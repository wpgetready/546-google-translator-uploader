//TODO: Change the app's name to ASO Translator
//TODO: Modify the app to hide or protect sensitive data for further demos, instead changing videos to hide things.

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import javax.swing.SwingUtilities;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.swing.JComponent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import javax.swing.JFrame;
import javax.swing.BorderFactory;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultCaret;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import java.awt.GridLayout;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Main extends JPanel
{
    private static final long serialVersionUID = 1L;
    private static String sCfgFile;
    JLabel infoLabel;
    JComboBox sectionList;
    JCheckBox chkCL;
    private boolean shouldCapitalize;
    private boolean shouldNotTranslateTitle;
    private boolean shouldNotTranslateShort;
    private boolean shouldNotTranslateLong;
    private int nSelectedIdx;
    private String sSelectedName;
    private JTextArea textAreaLog;
    private JTextField jTitle;
    private JTextField jtf;
    private JTextField jShortDesc;
    private JTextArea jLongDesc;
    private JButton btnTrans;
    private JButton btnTransAll;
    private JButton btnStart;
    private JButton btnSave;
    private JButton btnAddLang;
    private JButton btnDelLang;
    private JButton btnClearLog;
    private JButton btnValidate;
    boolean testActionListenerActive;
    private JCheckBox chkDoNotTitle;
    private JCheckBox chkDoNotShort;
    private JCheckBox chkDoNotLong;
    private DataPack data;
    
    private void initCombo() {
        this.nSelectedIdx = 0;
        this.sectionList.setSelectedIndex(this.nSelectedIdx);
        this.sSelectedName = (String)this.sectionList.getSelectedItem();
    }
    
    public Main() {
        this.shouldCapitalize = true;
        this.shouldNotTranslateTitle = false;
        this.shouldNotTranslateShort = false;
        this.shouldNotTranslateLong = false;
        this.testActionListenerActive = true;
        this.setLayout(new BoxLayout(this, 1));
        this.loadConfigs();
        final JLabel lbl1 = new JLabel("PROJECT PACKAGENAME : " + this.data.getPackageName(), 2);
        this.add(lbl1);
        final JLabel lbl1xq = new JLabel("**ASO Uploader Translator V3.0.1**", 2);
        this.add(Box.createRigidArea(new Dimension(5, 15)));
        this.add(lbl1xq);
        final JLabel lbl2 = new JLabel("Source dir : " + this.data.getSourceProjectDirectoryName(), 0);
        lbl2.setFont(lbl2.getFont().deriveFont(2));
        this.add(lbl2);
        this.add(Box.createRigidArea(new Dimension(5, 15)));
        lbl2.setAlignmentX(0.0f);
        this.add(Box.createRigidArea(new Dimension(5, 15)));
        final JLabel lbl3 = new JLabel("Language :", 0);
        this.sectionList = new JComboBox(this.data.getLangComboNames());


        this.initCombo();
        this.sectionList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                if (Main.this.testActionListenerActive) {
                    Main.this.saveSelectedLanguage();
                    final JComboBox cb = (JComboBox)e.getSource();
                    Main.access$1(Main.this, (String)cb.getSelectedItem());
                    Main.access$2(Main.this, cb.getSelectedIndex());
                    Main.this.loadSelectedLanguage();
                }
            }
        });
        final Box bCombo = Box.createHorizontalBox();
        bCombo.setAlignmentX(0.0f);
        bCombo.add(lbl3);
        bCombo.add(Box.createRigidArea(new Dimension(5, 5)));
        bCombo.add(this.sectionList);
        bCombo.add(Box.createRigidArea(new Dimension(5, 5)));
        (this.btnAddLang = new JButton("Add")).setToolTipText("add more languages.");
        this.btnAddLang.setPreferredSize(new Dimension(65, 30));
        this.btnAddLang.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                final String[] choices = Main.this.data.getNotPresentLangComboNames();
                if (choices.length == 0) {
                    JOptionPane.showMessageDialog(null, "I'm sorry, you already have all possible languages!");
                    return;
                }
                final JList list = new JList(choices);


                final JPanel panel = new JPanel(new GridLayout(1, 2));
                final JScrollPane jscr = new JScrollPane(list);
                jscr.setPreferredSize(new Dimension(150, 510));
                panel.add(jscr);
                final int x = JOptionPane.showConfirmDialog(null, panel, "Add new language(s)", 2);
                final int[] selix = list.getSelectedIndices();
                if (x == 0) {
                    Main.this.saveSelectedLanguage();
                    for (int i = 0; i < selix.length; ++i) {
                        final String s = choices[selix[i]];
                        Main.this.data.addLangByName(s);
                    }
                    Main.this.testActionListenerActive = false;
                    Main.this.sectionList.setModel(new DefaultComboBoxModel<String>(Main.this.data.getLangComboNames()));
                    Main.this.initCombo();
                    Main.this.loadSelectedLanguage();
                    Main.this.testActionListenerActive = true;
                }
            }
        });
        bCombo.add(this.btnAddLang);
        (this.btnDelLang = new JButton("Del")).setToolTipText("remove currently selected language.");
        this.btnDelLang.setPreferredSize(new Dimension(65, 30));
        this.btnDelLang.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                if (Main.this.nSelectedIdx > 0) {
                    final int dialogButton = 0;
                    final int dialogResult = JOptionPane.showConfirmDialog(null, "Do you really want to remove this language?", "Question", dialogButton);
                    if (dialogResult == 0) {
                        Main.this.testActionListenerActive = false;
                        Main.this.saveSelectedLanguage();
                        Main.this.data.delLang(Main.this.nSelectedIdx);
                        Main.access$2(Main.this, -1);
                        Main.this.sectionList.setModel(new DefaultComboBoxModel<String>(Main.this.data.getLangComboNames()));
                        Main.this.initCombo();
                        Main.this.loadSelectedLanguage();
                        Main.this.testActionListenerActive = true;
                    }
                }
            }
        });
        bCombo.add(this.btnDelLang);
        this.add(bCombo);
        final JLabel lblTitle = new JLabel("Title :", 0);
        this.jTitle = new JTextField(60);
        (this.chkCL = new JCheckBox("Capitalize first letters", true)).addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                Main.access$7(Main.this, e.getStateChange() == 1);
            }
        });
        final Box boxTitle = Box.createHorizontalBox();
        boxTitle.setAlignmentX(0.0f);
        boxTitle.add(lblTitle);
        boxTitle.add(Box.createRigidArea(new Dimension(5, 25)));
        boxTitle.add(this.jTitle);
        boxTitle.add(this.chkCL);
        this.add(boxTitle);
        this.chkDoNotTitle = new JCheckBox("Skip title", false);
        this.chkDoNotShort = new JCheckBox("Skip short desc", false);
        this.chkDoNotLong = new JCheckBox("Skip long desc", false);
        final Box boxExclusions = Box.createHorizontalBox();
        boxExclusions.setAlignmentX(0.0f);
        boxExclusions.add(this.chkDoNotTitle);
        boxExclusions.add(this.chkDoNotShort);
        boxExclusions.add(this.chkDoNotLong);
        this.chkDoNotTitle.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                Main.access$8(Main.this, e.getStateChange() == 1);
            }
        });
        this.chkDoNotShort.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                Main.access$9(Main.this, e.getStateChange() == 1);
            }
        });
        this.chkDoNotLong.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                Main.access$10(Main.this, e.getStateChange() == 1);
            }
        });
        this.add(boxExclusions);
        final JLabel lbl3C = new JLabel("Short description :", 0);
        this.jShortDesc = new JTextField(30);
        final Box bCombo2 = Box.createHorizontalBox();
        bCombo2.setAlignmentX(0.0f);
        bCombo2.add(lbl3C);
        bCombo2.add(Box.createRigidArea(new Dimension(5, 5)));
        bCombo2.add(this.jShortDesc);
        this.add(bCombo2);
        final JLabel lbl4 = new JLabel("Long description :", 2);
        lbl4.setAlignmentX(0.0f);
        this.add(lbl4);
        this.jLongDesc = new JTextArea(18, 80);
        final JScrollPane scrollPane = new JScrollPane(this.jLongDesc);
        scrollPane.setVerticalScrollBarPolicy(22);
        scrollPane.setAlignmentX(0.0f);
        this.jLongDesc.setEditable(true);
        this.jLongDesc.setLineWrap(true);
        this.add(scrollPane);
        final JLabel lbl5 = new JLabel("Log :", 2);
        lbl5.setAlignmentX(0.0f);
        this.add(lbl5);
        this.textAreaLog = new JTextArea(15, 80);
        final DefaultCaret caret = (DefaultCaret)this.textAreaLog.getCaret();
        caret.setUpdatePolicy(2);
        final JScrollPane scrollPaneLog = new JScrollPane(this.textAreaLog);
        scrollPaneLog.setVerticalScrollBarPolicy(22);
        scrollPaneLog.setAlignmentX(0.0f);
        this.textAreaLog.setEditable(false);
        this.add(scrollPaneLog);
        this.add(Box.createRigidArea(new Dimension(5, 15)));
        (this.btnTrans = new JButton("Translate Selected")).setPreferredSize(new Dimension(177, 30));
        this.btnTrans.setToolTipText("call Google Translate and translate currently selected language only.");
        this.btnTrans.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                Main.this.doStartTranslate(false);
            }
        });
        (this.btnTransAll = new JButton("Translate All")).setPreferredSize(new Dimension(177, 30));
        this.btnTransAll.setToolTipText("call Google Translate and translate all languages added to current project (via Add button).");
        this.btnTransAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                Main.this.doStartTranslate(true);
            }
        });
        (this.btnSave = new JButton("Save .gpup")).setPreferredSize(new Dimension(177, 30));
        this.btnSave.setToolTipText("save current translation results to your .gpup file.");
        this.btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                Main.this.saveSelectedLanguage();
                if (Main.this.data.saveToFile(Main.sCfgFile) != 0) {
                    JOptionPane.showMessageDialog(null, "Error saving file.", "Error", -1, null);
                }
            }
        });
        (this.btnStart = new JButton("Upload Translations to Google Play")).setPreferredSize(new Dimension(177, 30));
        this.btnStart.setEnabled(false); //disable button , it will be enabled only after validation
        this.btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                Main.this.doStartUpload();
            }
        });
        this.btnStart.setToolTipText("upload all your translations to Google Play dev console using GOOGLE API");

        (this.btnClearLog = new JButton("Clear Log")).setPreferredSize(new Dimension(177, 30));
        //btnCreate(this.btnClearLog,"Clear Log",177,30,"Clear text logs");
        this.btnClearLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (textAreaLog!=null) {
                    //https://stackoverflow.com/questions/15798532/how-to-clear-jtextarea
                    //Quite absurd...
                    textAreaLog.selectAll();
                    textAreaLog.replaceSelection("");
                }
            }
        });

        (this.btnValidate=new JButton("Validate all Data")).setPreferredSize(new Dimension(177,30));
        this.btnValidate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validateUpload(true);
            }
        });

        final Box boxBtns = Box.createHorizontalBox();
        boxBtns.setAlignmentX(0.0f);
        boxBtns.add(this.btnTrans);
        boxBtns.add(Box.createRigidArea(new Dimension(5, 5)));
        boxBtns.add(this.btnTransAll);
        boxBtns.add(Box.createRigidArea(new Dimension(15, 5)));
        boxBtns.add(this.btnSave);
        boxBtns.add(Box.createRigidArea(new Dimension(5, 5)));
        boxBtns.add(this.btnValidate);
        boxBtns.add(Box.createRigidArea(new Dimension(5, 5)));
        boxBtns.add(this.btnStart);
        boxBtns.add(Box.createRigidArea(new Dimension(5, 5)));
        boxBtns.add((this.btnClearLog));
        this.add(boxBtns);
        this.loadSelectedLanguage();
        this.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 24));

    }

    private void validateUpload(boolean showMsg) {
        //Save current language in case you made changes
        //(Note: changing language automatically saves changes also)
        saveSelectedLanguage();
        final String s = data.validation();
        if (!s.isEmpty()) {
            if (showMsg) {
                Main.this.addLog("WARNING! Check following issues first.(Upload aborted).\n");
                Main.this.addLog(s);
            }
            btnStart.setEnabled(false);
        } else {
            if (showMsg){
                Main.this.addLog("VALIDATION PASS: Ready for starting upload.\n");
            }
            btnStart.setEnabled(true);
        }
    }

    //Experimental, failed (why?)
    private void btnCreate (JButton btn, String txt, int width,int height,String tooltip) {
        (btn= new JButton(txt)).setPreferredSize(new Dimension(width,height));
        btn.setToolTipText(tooltip);
    }
    private void saveSelectedLanguage() {
        if (this.nSelectedIdx >= 0) {
            this.data.setLang_Title(this.nSelectedIdx, this.jTitle.getText());
            this.data.setLang_ShortDesc(this.nSelectedIdx, this.jShortDesc.getText());
            this.data.setLang_LongDesc(this.nSelectedIdx, this.jLongDesc.getText());
        }
    }
    
    private void loadSelectedLanguage() {
        this.jTitle.setText(this.data.getLang_Title(this.nSelectedIdx));
        this.jShortDesc.setText(this.data.getLang_ShortDesc(this.nSelectedIdx));
        this.jLongDesc.setText(this.data.getLang_LongDesc(this.nSelectedIdx));
    }
    
    private static void createAndShowGUI() {
        final JFrame frame = new JFrame("Main");
        frame.setDefaultCloseOperation(3);
        frame.setTitle("(Google Play) APK Translator & Uploader v2.2");
        final JComponent newContentPane = new Main();
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                Main.sCfgFile.length();
                e.getWindow().dispose();
            }
        });
        frame.pack();
        frame.setVisible(true);
    }
    
    public DataPack getData() {
        return this.data;
    }
    
    private void loadConfigs() {
        (this.data = new DataPack()).loadFromFile(Main.sCfgFile);
    }
    
    public void addLog(final String s) {
        this.textAreaLog.append(s);
        this.textAreaLog.setCaretPosition(this.textAreaLog.getDocument().getLength());
    }
    
    private void disableAllWidgets() {
        this.sectionList.setEnabled(false);
        this.btnStart.setEnabled(false);
        this.btnTrans.setEnabled(false);
        this.btnTransAll.setEnabled(false);
        this.btnSave.setEnabled(false);
        this.btnAddLang.setEnabled(false);
        this.btnDelLang.setEnabled(false);
        this.jTitle.setEnabled(false);
        this.jShortDesc.setEnabled(false);
        this.jLongDesc.setEnabled(false);
    }
    
    private void enableAllWidgets() {
        this.sectionList.setEnabled(true);
        validateUpload(false); //Validate and do not Show button if data needs to be improved
        this.btnTrans.setEnabled(true);
        this.btnTransAll.setEnabled(true);
        this.btnSave.setEnabled(true);
        this.btnAddLang.setEnabled(true);
        this.btnDelLang.setEnabled(true);
        this.jTitle.setEnabled(true);
        this.jShortDesc.setEnabled(true);
        this.jLongDesc.setEnabled(true);
    }
    
    public void doStartTranslate(final boolean all) {
        final DataPack data = this.data;
        this.saveSelectedLanguage();
        this.disableAllWidgets();
        new Thread() {
            @Override
            public void run() {
                try {
                    if (!all && Main.this.nSelectedIdx ==0) {
                        Main.this.addLog("Select another language from the combo to translate.I Can't translate DEFAULT language...Aborting!\n");
                        Main.this.loadSelectedLanguage();
                        Main.this.enableAllWidgets();
                        return;
                    }
                    final GTrans2 gt = new GTrans2();
                    gt.initTranslation();
                    Main.this.addLog("Starting automatic translation...\n");
                    final String sBaseTitle = data.getLang_Title(0);
                    final String sBaseShortDesc = data.getLang_ShortDesc(0);
                    final String sBaseLongDesc = data.getLang_LongDesc(0);
                    String[] out=null;
                    for (int i = 1; i < data.getLang_ArrCount(); ++i) {
                        if (all || i == Main.this.nSelectedIdx) {
                            String sTitle = data.getLang_Title(i);
                            String sShortDesc = data.getLang_ShortDesc(i);
                            String sLongDesc = data.getLang_LongDesc(i);
                            final String destLangCode = data.getLang_GTCode(i);
                            boolean flag = false;
                            for (int j = 0; j < i && !flag; ++j) {
                                if (data.getLang_GTCode(j) == destLangCode) {
                                    data.setLang_Title(i, data.getLang_Title(j));
                                    data.setLang_ShortDesc(i, data.getLang_ShortDesc(j));
                                    data.setLang_LongDesc(i, GTrans2.stripDNT_Tags(data.getLang_LongDesc(j)));
                                    flag = true;
                                    Main.this.addLog("Translation copied " + data.getLang_Code(j) + " => " + data.getLang_Code(i) + "\n");
                                }
                            }
                            if (!flag) {

                                out=gt.doTranslate(destLangCode, sBaseTitle, sBaseShortDesc, sBaseLongDesc);

                                sTitle = out[0];
                                if (Main.this.shouldCapitalize) {
                                    sTitle = Main.this.capitalizeTitle(sTitle);
                                }
                                if (Main.this.shouldNotTranslateTitle) {
                                    data.setLang_Title(i, sBaseTitle);
                                }
                                else {
                                    data.setLang_Title(i, sTitle);
                                }
                                sShortDesc = out[1];
                                if (Main.this.shouldCapitalize) {
                                    sShortDesc = Main.this.capitalize(sShortDesc);
                                }
                                if (Main.this.shouldNotTranslateShort) {
                                    data.setLang_ShortDesc(i, sBaseShortDesc);
                                }
                                else {
                                    data.setLang_ShortDesc(i, sShortDesc);
                                }
                                sLongDesc = convertArrayToStringMethod(2,out);
                                if (Main.this.shouldCapitalize) {
                                    sLongDesc = Main.this.capitalize(sLongDesc);
                                }
                                if (Main.this.shouldNotTranslateLong) {
                                    data.setLang_LongDesc(i, sBaseLongDesc);
                                }
                                else {
                                    data.setLang_LongDesc(i, sLongDesc);
                                }
                                Main.this.addLog("Translated to " + data.getLang_Code(i) + "\n");
                            }
                        }
                    }
                    Main.this.addLog("Translation finished.\n");
                }
                catch (Exception e) {
                    final StringWriter sw = new StringWriter();
                    final PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace(pw);
                    Main.this.addLog(sw.toString());
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Main.this.loadSelectedLanguage();
                        Main.this.enableAllWidgets();
                    }
                });
            }
        }.start();
    }

    public String convertArrayToStringMethod(int startFrom,String[] strArray) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = startFrom; i < strArray.length; i++) {
            stringBuilder.append(strArray[i]+"\n");
        }
        return stringBuilder.toString();
    }
    
    public void doStartUpload() {
        final DataPack data = this.data;
        this.saveSelectedLanguage();
        this.disableAllWidgets();
        new Thread() {
            @Override
            public void run() {
                try {
                    final String s = data.validation();
                    if (!s.isEmpty()) {
                        Main.this.addLog("WARNING! Check following issues first.(Upload aborted)\n");
                        Main.this.addLog(s);
                    }
                    else {
                        Main.this.addLog("Starting upload...\n");
                        final BasicUploadApk u = new BasicUploadApk(data, Main.this.shouldCapitalize);
                        u.doStart(Main.this);
                    }
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            Main.this.enableAllWidgets();
                        }
                    });
                    Main.this.addLog("Finished.\n");
                }
                catch (Exception e) {
                    final StringWriter sw = new StringWriter();
                    final PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace(pw);
                    Main.this.addLog(sw.toString());
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            Main.this.enableAllWidgets();
                        }
                    });
                }
            }
        }.start();
    }
    
    public static void copyFile(final File sourceFile, final File destFile) throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }
        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0L, source.size());
        }
        finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
    }
    
    public String capitalize(final String s1) {
        if (s1.length() == 0) {
            return "";
        }
        final String s2 = String.valueOf(s1.substring(0, 1).toUpperCase()) + s1.substring(1);
        return s2;
    }
    
    public String capitalizeTitle(final String s1) {
        if (s1.length() == 0) {
            return "";
        }
        String s2 = s1.substring(0, 1).toUpperCase();
        for (int ix = 1; ix < s1.length(); ++ix) {
            if (s1.charAt(ix - 1) == ' ') {
                s2 = String.valueOf(s2) + s1.substring(ix, ix + 1).toUpperCase();
            }
            else {
                s2 = String.valueOf(s2) + s1.substring(ix, ix + 1);
            }
        }
        return s2;
    }
    
    public static void main(final String[] args) {
        if (i13() == 0) {
            System.exit(-1);
        }
        if (args.length == 0) {
            Main.sCfgFile = "default.gpup";
        }
        else {
            Main.sCfgFile = args[0];
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }


    
    public static int i13() {
        return 1;
    }
    
    static /* synthetic */ void access$1(final Main main, final String sSelectedName) {
        main.sSelectedName = sSelectedName;
    }
    
    static /* synthetic */ void access$2(final Main main, final int nSelectedIdx) {
        main.nSelectedIdx = nSelectedIdx;
    }
    
    static /* synthetic */ void access$7(final Main main, final boolean shouldCapitalize) {
        main.shouldCapitalize = shouldCapitalize;
    }
    
    static /* synthetic */ void access$8(final Main main, final boolean shouldNotTranslateTitle) {
        main.shouldNotTranslateTitle = shouldNotTranslateTitle;
    }
    
    static /* synthetic */ void access$9(final Main main, final boolean shouldNotTranslateShort) {
        main.shouldNotTranslateShort = shouldNotTranslateShort;
    }
    
    static /* synthetic */ void access$10(final Main main, final boolean shouldNotTranslateLong) {
        main.shouldNotTranslateLong = shouldNotTranslateLong;
    }
}
