/*
Dependencies

        <dependency>
            <groupId>com.google.apis</groupId>
            <artifactId>google-api-services-androidpublisher</artifactId>
            <version>v3-rev46-1.25.0</version>
        </dependency>

        <dependency>
            <groupId>com.google.api.client</groupId>
            <artifactId>google-api-client-repackaged-com-google-common-base</artifactId>
            <version>1.2.3-alpha</version>
        </dependency>

 */

import java.security.GeneralSecurityException;
import java.io.IOException;
import com.google.api.services.androidpublisher.AndroidPublisher;
import com.google.api.services.androidpublisher.model.Listing;
import com.google.api.services.androidpublisher.model.AppEdit;
import com.google.api.client.repackaged.com.google.common.base.Preconditions;
//import com.google.api.client.repackaged.com.google.common.base.Strings;
//import com.google.appengine.repackaged.com.google.common.base.Strings;


import java.util.List;
import java.io.File;
import java.util.ArrayList;

import com.google.common.base.Strings;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;

public class BasicUploadApk
{
    DataPack data;
    boolean shouldCapitalize;
    private static final Log log;
    private static final String TRACK_ALPHA = "beta";
    
    static {
        log = LogFactory.getLog((Class)BasicUploadApk.class);
    }
    
    BasicUploadApk(final DataPack dp, final boolean shouldCapitalize) {
        this.data = dp;
        this.shouldCapitalize = shouldCapitalize;
    }
    
    private int searchP12File(final Main m) {
        String sFound = "";
        final List<String> textFiles = new ArrayList<String>();
        final File dir = new File(".");
        File[] listFiles;
        for (int length = (listFiles = dir.listFiles()).length, i = 0; i < length; ++i) {
            final File file = listFiles[i];
            if (file.getName().endsWith(".p12")) {
                if (!sFound.isEmpty()) {
                    m.addLog(String.format("Error. Found next P12 key: %s. I need only one P12 file.", sFound));
                    return -1;
                }
                sFound = file.getName();
                m.addLog(String.format("Found P12 key: %s", sFound));
                AndroidPublisherHelper.SRC_RESOURCES_KEY_P12 = sFound;
            }
        }
        return 0;
    }
    
    void doStart(final Main m) throws IOException, GeneralSecurityException {
        if (-1 == this.searchP12File(m)) {
            return;
        }
        Preconditions.checkArgument(!Strings.isNullOrEmpty(this.data.getPackageName()), (Object)"PackageName cannot be null or empty!");
        final AndroidPublisher service = AndroidPublisherHelper.init(this.data.getGoogleServiceProjectName(), this.data.getGoogleServiceAccountEmail());
        final AndroidPublisher.Edits edits = service.edits();
        final AndroidPublisher.Edits.Insert editRequest = edits.insert(this.data.getPackageName(), (AppEdit)null);
        final AppEdit edit = (AppEdit)editRequest.execute();
        final String editId = edit.getId();
        BasicUploadApk.log.info((Object)String.format("Created edit with id: %s", editId));
        for (int i = 0; i < this.data.getLang_ArrCount(); ++i) {
            if (this.data.getLang_Locale(i).length() > 0) {
                final Listing newUsListing = new Listing();
                final String sLongDesc = tagsclean(this.data.getLang_LongDesc(i));
                newUsListing.setTitle(this.capitalizeTitle(this.data.getLang_Title(i))).setFullDescription(this.capitalize(sLongDesc)).setShortDescription(this.capitalize(this.data.getLang_ShortDesc(i)));
                final AndroidPublisher.Edits.Listings.Update updateUSListingsRequest = edits.listings().update(this.data.getPackageName(), editId, this.data.getLang_Locale(i), newUsListing);
                final Listing updatedUsListing = (Listing)updateUSListingsRequest.execute();
                m.addLog(String.format("Created new " + this.data.getLang_Locale(i) + " app listing with title: %s", updatedUsListing.getTitle()));
            }
        }
        final AndroidPublisher.Edits.Commit commitRequest = edits.commit(this.data.getPackageName(), editId);
        final AppEdit appEdit = (AppEdit)commitRequest.execute();
        BasicUploadApk.log.info((Object)String.format("App edit with id %s has been comitted", appEdit.getId()));
        m.addLog(String.format("App edit with id %s has been comitted.", appEdit.getId()));
    }
    
    public String capitalize(final String s1) {
        if (!this.shouldCapitalize) {
            return s1;
        }
        if (s1.length() == 0) {
            return "";
        }
        final String s2 = String.valueOf(s1.substring(0, 1).toUpperCase()) + s1.substring(1);
        return s2;
    }
    
    public static String tagsclean(String s1) {
        s1 = s1.replace("<#-#>", "");
        s1 = s1.replace("<<#", "");
        s1 = s1.replace("#>>", "");
        return s1;
    }
    
    public String capitalizeTitle(final String s1) {
        if (!this.shouldCapitalize) {
            return s1;
        }
        if (s1.length() == 0) {
            return "";
        }
        String s2 = s1.substring(0, 1).toUpperCase();
        for (int ix = 1; ix < s1.length(); ++ix) {
            if (s1.charAt(ix - 1) == ' ') {
                s2 = String.valueOf(s2) + s1.substring(ix, ix + 1).toUpperCase();
            }
            else {
                s2 = String.valueOf(s2) + s1.substring(ix, ix + 1);
            }
        }
        return s2;
    }
}
