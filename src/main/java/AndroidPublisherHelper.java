
/*
Dependencies:
        <dependency>
            <groupId>com.google.apis</groupId>
            <artifactId>google-api-services-androidpublisher</artifactId>
            <version>v3-rev46-1.25.0</version>
        </dependency>

          <dependency>
            <groupId>com.google.api.client</groupId>
            <artifactId>google-api-client-repackaged-com-google-common-base</artifactId>
            <version>1.2.3-alpha</version>
        </dependency>

 */
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
//import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.api.client.repackaged.com.google.common.base.Preconditions;
import javax.annotation.Nullable;
import com.google.api.services.androidpublisher.AndroidPublisher;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.common.base.Strings;
import org.apache.commons.logging.LogFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import org.apache.commons.logging.Log;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.io.File;
import java.util.Collection;
import java.util.Collections;

public class AndroidPublisherHelper
{
    private static final Log log;
    static final String MIME_TYPE_APK = "application/vnd.android.package-archive";
    static String SRC_RESOURCES_KEY_P12;
    private static final JsonFactory JSON_FACTORY;
    private static HttpTransport HTTP_TRANSPORT;
    
    static {
        log = LogFactory.getLog((Class)AndroidPublisherHelper.class);
        AndroidPublisherHelper.SRC_RESOURCES_KEY_P12 = "key.p12";
        JSON_FACTORY = (JsonFactory)JacksonFactory.getDefaultInstance();
    }
    
    private static Credential authorizeWithServiceAccount(final String serviceAccountEmail) throws GeneralSecurityException, IOException {
        AndroidPublisherHelper.log.info((Object)String.format("Authorizing using Service Account: %s", serviceAccountEmail));
        final GoogleCredential credential = new GoogleCredential.Builder().setTransport(AndroidPublisherHelper.HTTP_TRANSPORT).setJsonFactory(AndroidPublisherHelper.JSON_FACTORY).setServiceAccountId(serviceAccountEmail).setServiceAccountScopes((Collection)Collections.singleton("https://www.googleapis.com/auth/androidpublisher")).setServiceAccountPrivateKeyFromP12File(new File(AndroidPublisherHelper.SRC_RESOURCES_KEY_P12)).build();
        return (Credential)credential;
    }
    
    protected static AndroidPublisher init(final String applicationName) throws Exception {
        return init(applicationName, null);
    }
    
    protected static AndroidPublisher init(final String applicationName, @Nullable final String serviceAccountEmail) throws IOException, GeneralSecurityException {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(applicationName), (Object)"applicationName cannot be null or empty!");
        newTrustedTransport();
        if (serviceAccountEmail == null || serviceAccountEmail.isEmpty()) {
            return null;
        }
        final Credential credential = authorizeWithServiceAccount(serviceAccountEmail);
        return new AndroidPublisher.Builder(AndroidPublisherHelper.HTTP_TRANSPORT, AndroidPublisherHelper.JSON_FACTORY, (HttpRequestInitializer)credential).setApplicationName(applicationName).build();
    }
    
    private static void newTrustedTransport() throws GeneralSecurityException, IOException {
        if (AndroidPublisherHelper.HTTP_TRANSPORT == null) {
            AndroidPublisherHelper.HTTP_TRANSPORT = (HttpTransport)GoogleNetHttpTransport.newTrustedTransport();
        }
    }
}
