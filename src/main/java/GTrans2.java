//This current class mimics GTrans functionality,
//I'm going to mark with (+) the advantages  and with (-) the relatives disvantages
//(+)It is using Google Cloud Translate API: this means a STANDARIZED way of working
//and also this software would be more stable and durable through time
//(+)faster
//(-) Google API Cloud costs $20 for every million characters.

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

import java.io.IOException;
import java.util.*;

/*
Dependencies
        <dependency>
            <groupId>com.google.cloud</groupId>
            <artifactId>google-cloud-translate</artifactId>
            <version>1.55.0</version>
        </dependency>
 */

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.Translate.TranslateOption;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

public class GTrans2
{
    public static String DNT_1 = "<#do_not_translate>";
    public static String DNT_1_CLOSE = "</#do_not_translate>";
    public static String DNT_2 = "<#dnt>";
    public static String DNT_2_CLOSE = "</#dnt>";

    GoogleCredential credential;
    Translate translate;

    public void initTranslation() {
        try {
            credential = GoogleCredential.getApplicationDefault();
            translate = TranslateOptions.getDefaultInstance().getService();
        } catch (IOException ioe) {
            System.out.println("Error: " + ioe.getMessage()); //TODO: To Improve
        }
    }

    //Suppresses all DNT tags. Useful when we have english languages needn't to be translated
    public static String stripDNT_Tags(String Content) {
        String result =Content.replace(DNT_1,"").replace(DNT_1_CLOSE,"").
                replace(DNT_2,"").replace(DNT_2_CLOSE,"");
        return result;
    }

    //This is the main mehtod
    public  String[] doTranslate(final String destLangCode, String title, String shortDesc, final String longDesc) {
        String base = title +"\n" + shortDesc + "\n" +longDesc;
        return doTranslateV2(base,destLangCode);
    }

/*20190319: ADVANCED improvement of doTranslate, using special tags split translation text for avoiding translation section. */
    public String[] doTranslateV2(String txtToTranslate,String destLangCode){
        System.out.print("Starting doTranslateV2...");
        List<TranslateLine> linesToTranslate =null;
        try
        {
            linesToTranslate = getLinesToTranslate(txtToTranslate);
        }  catch(Exception e) {
            System.out.print("Translation aborted: ...." + e.getMessage());
            return null;
        }

        //Iterate  over the linestToTranslate and translate them.
        for (TranslateLine tl: linesToTranslate) {
            //Only translate translatable sections, otherwise ignore it.
            if (tl.getTranslate()) {
                List<Translation> translations = translate.translate(tl.getLines(),
                        TranslateOption.sourceLanguage("en"),
                        TranslateOption.targetLanguage(destLangCode),TranslateOption.format("html"));
                //tl.setLines(Arrays.asList(translations.get(0).getTranslatedText().split("\\n"))); //returns a string instead an array.
                //Add all lines translated
                tl.clearLines(); //delete old lines, add translated ones
                for(int i=0;i<translations.size();i++){
                    tl.addLine(translations.get(i).getTranslatedText());
                }
            }
            System.out.println("Translating to ..." + destLangCode);
        }
        //The translation process was ended,we need to reassemble all the text including translated and not translated text
        List<String> result = new ArrayList<String>();

        for (TranslateLine tl:linesToTranslate) {
            for (String line: tl.getLines()) {
                result.add(line);
            }
        }
        String[] r=result.toArray(new String[0]);
        return   r;
    }

    /**
     * Simplified and very improved version for translation
     * Requirements: To isolate data which won't be translated, it will need to be surrounded by '<#do_not_translate>'...'</#do_not_translate>'
     * or '<#dnt>...</#dnt>
     * This text needs to be alone in one line
     * Correct:
     * <#dnt>
     *     ...
     * </#dnt>
     * Incorrect
     * <#dnt>  <div>Hello!</div>
     *     ...
     * </#dnt> <span>incorrect!</span>
     * the output will be a collection of collections where setTranslate says if to translate or not.
     * @param Content
     * @return
     */

    public List<TranslateLine> getLinesToTranslate(String Content) throws Exception {
        //Convert all lines as a List
        // Reference: https://stackoverflow.com/questions/10455738/converting-a-string-with-breaks-to-an-arraylist
        int openCounter=0;
        List<String> lines = null;
        if (Content ==null) {
            System.out.println("Error getLineToTranslate input null...: ");
            return null;
        }
        lines = Arrays.asList(Content.split("\\n"));

        List<TranslateLine> tlines = new ArrayList<TranslateLine>();
        TranslateLine tl;
        tl = new TranslateLine();
        tl.setTranslate(true); //Assuming we started without finding a #dnt tag...
        for (String line: lines) {
            if ((line.trim().toLowerCase().equals(DNT_1) ) | (line.trim().toLowerCase().equals(DNT_2)))
            {
                openCounter++; //open counter detector
                //Add groups ONLY if they have at least one line, avoiding empty objects
                if (openCounter>1) {
                    throw  new Exception("getLinesToTranslate: Detected possible nested or repeated open tags.Aborted");
                }
                if (tl.lines.size()!=0) {
                    tlines.add(tl); //Add the collection line to the general collection
                }
                tl = new TranslateLine();                 //Start new collection
                tl.setTranslate(false); //Make sure this collection won't be translated
                continue; //process next line do nothing with this one
            }

            if ((line.trim().toLowerCase().equals(DNT_1_CLOSE)) | (line.trim().toLowerCase().equals(DNT_2_CLOSE)))
            {
                openCounter--;
                //Allow adding groups only if they have at least one line.

                if (openCounter<0) {
                    throw  new Exception("getLinesToTranslate: Detected possible nested or repeated closing tags.Aborted");
                }
                if (tl.lines.size()!=0) {
                    tlines.add(tl);
                }
                tl = new TranslateLine();

                tl.setTranslate(true); //Make sure this collection will be translated
                continue; //process next line do nothing with this one
            }
            tl.addLine(line);
        }
        if (tl.lines.size()!=0) {
            tlines.add(tl);
        }
        //  tlines.add(tl);
        //Check counter quantity
        if (openCounter !=0){
            throw  new Exception("getLinesToTranslate: incorrect open/close counter do_not_traslate tags");
        }
        return tlines;
    }
}